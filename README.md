# PlasticityTools

[![travis status](https://travis-ci.org/KeitaNakamura/PlasticityTools.jl.svg?branch=master)](https://travis-ci.org/KeitaNakamura/PlasticityTools.jl)
[![appveyor status](https://ci.appveyor.com/api/projects/status/nxo38kc6nenuw6bh/branch/master?svg=true)](https://ci.appveyor.com/project/KeitaNakamura/plasticitytools-jl/branch/master)

[![coveralls status](https://img.shields.io/coveralls/github/KeitaNakamura/PlasticityTools.jl/master.svg?label)](https://coveralls.io/github/KeitaNakamura/PlasticityTools.jl?branch=master)
[![codecov status](http://codecov.io/github/KeitaNakamura/PlasticityTools.jl/coverage.svg?branch=master)](http://codecov.io/github/KeitaNakamura/PlasticityTools.jl?branch=master)

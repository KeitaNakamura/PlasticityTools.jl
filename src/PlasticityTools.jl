module PlasticityTools

using TensorArrays
using ForwardDiff

function stiffness(f::Real, F::Func, dϵ::TT) where {Func, T <: Real, L, TT <: SymmetricTensor{2, 3, T, L}}
    dFdx_1, dFdx_2 = F(one(T), TensorArrays.dualize(zero(dϵ)))
    _0 = zero(T)
    Dᵉ = TensorArrays.extract_derivative(dFdx_2, dϵ)
    f < _0 && return Dᵉ
    dfdγ = TensorArrays.extract_value(dFdx_1, dϵ)
    Dᵉ_dgdσ = TensorArrays.extract_value(dFdx_2, dϵ) # -Dᵉ ⊡ dgdσ
    dfdϵ = TensorArrays.extract_derivative(dFdx_1, dϵ)
    dγdϵ = -dfdγ^-1 * dfdϵ
    return dγdϵ ⊡ dϵ > _0 ? Dᵉ + Dᵉ_dgdσ ⊗ dγdϵ : Dᵉ
end

end # module

using PlasticityTools
using TensorArrays
using Functors
using Base.Test

const T2 = SymmetricTensor{2,3}
const T4 = SymmetricTensor{4,3}

@testset "stiffness" begin
    for T in (Float32, Float64)
        σ = rand(T2{T})
        ϵᵖ = rand(T2{T})
        dϵ = rand(T2{T})
        dgdσ = rand(T2{T})
        Dᵉ = rand(T4{T})
        f = (σ, ϵᵖ) -> σ ⊡ σ + trace(ϵᵖ)
        dfdσ, dfdϵᵖ = derivative(f, (σ, ϵᵖ))

        F = @functor function(dγ, dϵ; dfdσ, dfdϵᵖ, dgdσ, Dᵉ)
            dϵᵖ = dγ * dgdσ
            dσ = Dᵉ ⊡ (dϵ - dϵᵖ)
            return dfdσ ⊡ dσ + dfdϵᵖ ⊡ dϵᵖ, dσ
        end

        @test @inferred(PlasticityTools.stiffness(-one(T), F, dϵ)) ≈ Dᵉ
        @test @inferred(PlasticityTools.stiffness( one(T), F, dϵ)) ≈
              Dᵉ - Dᵉ ⊡ dgdσ ⊗ dfdσ ⊡ Dᵉ / (dfdσ ⊡ Dᵉ ⊡ dgdσ - dfdϵᵖ ⊡ dgdσ)
    end
end
